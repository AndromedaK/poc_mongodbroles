﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace poc_mongodbroles.Modelo
{
    public class Privilegios
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombrePrivilegio")]
        public string Nombre { get; set; }

        public bool isChecked { get; set; }


        /*
        public static List<Privilegios> RetornarPrivilegios()
        {
            List<Privilegios> listaPrivilegios = new List<Privilegios>();
            listaPrivilegios.Add(new Privilegios("Crear"));
            listaPrivilegios.Add(new Privilegios("Eliminar"));
            listaPrivilegios.Add(new Privilegios("Actualizar"));
            listaPrivilegios.Add(new Privilegios("Leer"));
            return listaPrivilegios;
        }
        */

        public Privilegios( string Nombre)
        {
            this.Nombre = Nombre;
            this.isChecked = true;
        }

        public Privilegios(string Nombre , ObjectId Id)
        {
            this.Id = Id;
            this.Nombre = Nombre;
            this.isChecked = true;

        }

        public Privilegios()
        {
            this.isChecked = true;

        }


    }
}
