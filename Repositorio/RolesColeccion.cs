﻿using MongoDB.Bson;
using MongoDB.Driver;
using poc_mongodbroles.Modelo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace poc_mongodbroles.Repositorio
{
    public class RolesColeccion : IRolesColeccion
    {
        internal MongoDB mongoDB = new MongoDB("ControlTareas");

        private IMongoCollection<Roles> mongoCollection;
        public RolesColeccion()
        {
            mongoCollection = mongoDB.db.GetCollection<Roles>("roles");
        }
        public async Task DeleteRol(string id)
        {
            var filter = Builders<Roles>.Filter.Eq(s => s.Id, new ObjectId(id));
            await mongoCollection.DeleteOneAsync(filter);
        }

        public async Task<List<Roles>> GetAllRol()
        {
            return await mongoCollection.FindAsync(new BsonDocument()).Result.ToListAsync();
        }

        public  async Task InsertRol(Roles roles)
        {
            await mongoCollection.InsertOneAsync(roles);
        }

        public async Task UpdateRol(Roles roles)
        {
            var filter = Builders<Roles>.Filter.Eq(s => s.Id, roles.Id);
            await mongoCollection.ReplaceOneAsync(filter, roles);

        }
    }
}
