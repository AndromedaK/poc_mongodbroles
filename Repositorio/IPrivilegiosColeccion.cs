﻿using poc_mongodbroles.Modelo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace poc_mongodbroles.Repositorio
{
    public interface IPrivilegiosColeccion
    {
       Task<List<Privilegios>> GetAllPrivilegios();
    }
}
