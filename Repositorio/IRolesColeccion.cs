﻿using poc_mongodbroles.Modelo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace poc_mongodbroles.Repositorio
{
    public interface IRolesColeccion
    {
        Task InsertRol(Roles roles);
        Task UpdateRol(Roles roles);
        Task DeleteRol(string id);
        Task<List<Roles>> GetAllRol();

    }
}
