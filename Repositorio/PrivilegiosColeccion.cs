﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using poc_mongodbroles.Modelo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace poc_mongodbroles.Repositorio
{
    public class PrivilegiosColeccion : IPrivilegiosColeccion
    {

        internal MongoDB mongoDB = new MongoDB("ControlTareas");

        private IMongoCollection<Privilegios> mongoCollection;
        public PrivilegiosColeccion()
        {
            mongoCollection = mongoDB.db.GetCollection<Privilegios>("privilegios");
        }

        public async Task<List<Privilegios>> GetAllPrivilegios()
        {
            return await mongoCollection.FindAsync(new BsonDocument()).Result.ToListAsync();
        }
    }
}
