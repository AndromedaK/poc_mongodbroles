﻿using MahApps.Metro.Controls;
using poc_mongodbroles.Modelo;
using poc_mongodbroles.Repositorio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace poc_mongodbroles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private IPrivilegiosColeccion MetodosPrivilegios = new PrivilegiosColeccion();

        private IRolesColeccion MetodosRoles = new RolesColeccion();
        
        public MainWindow()
        {
            InitializeComponent();
            FillComboBox();
            FillDatGrid();
        }


        private async void FillComboBox() 
        {
            var items = await MetodosPrivilegios.GetAllPrivilegios();
            CmboxPrivilegios.ItemsSource = items;
        }

        private async void FillDatGrid()
        {
            var items = await MetodosRoles.GetAllRol();
            DtaGridRoles.ItemsSource = items;
        }

        private async void Buton_Crear(object sender, RoutedEventArgs e)
        {
            Roles roles = new Roles();
            roles.NombreRol = TxtNombreRol.Text;
            ArrayList privilegios = new ArrayList();
            foreach (var item  in CmboxPrivilegios.Items.OfType<Privilegios>())
            {
                bool isChecked = item.isChecked;
                if (isChecked)
                {
                    int contador =+1;
                    for (int i = 0; i < contador ; i++)
                    {
                        privilegios.Add(item.Nombre.ToString());
                    }                 
                }               
            }  
            roles.Privilegios =  privilegios;
            await MetodosRoles.InsertRol(roles);
            FillDatGrid();
        }



        private async void Buton_Actualizar(object sender, RoutedEventArgs e)
        {
            Roles roles = new Roles();
            ArrayList privilegios = new ArrayList();
            foreach (var item in CmboxPrivilegios.Items.OfType<Privilegios>())
            {
                bool isChecked = item.isChecked;
                if (isChecked)
                {
                    int contador = +1;
                    for (int i = 0; i < contador; i++)
                    {
                        privilegios.Add(item.Nombre.ToString());
                    }
                }
            }
            /*
            ArrayList privilegios = new ArrayList();
            privilegios.Add("crear");
            privilegios.Add("Eliminar");
            */
            roles.NombreRol = TxtNombreRol.Text;
            roles.Privilegios = privilegios;
            await MetodosRoles.UpdateRol(roles);
            FillDatGrid();

        }

        private void Buton_Eliminar(object sender, RoutedEventArgs e)
        {

        }


    }
}
